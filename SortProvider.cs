﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APS_04
{
    public abstract class SortProvider
    {
        public enum Provider
        {
            BucketSort,
            InsertionSort,
            MergeSort
        }

        public static ISorter GetSorter(Provider kind)
        {
            return kind switch
            {
                Provider.BucketSort => new BucketSorter(),
                Provider.InsertionSort => new InsertionSorter(),
                Provider.MergeSort => new MergeSorter(),
                _ => throw new ArgumentException(),
            };
        }
    }
}
