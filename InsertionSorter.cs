﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APS_04
{
    class InsertionSorter : ISorter
    {
        public override string GetName()
        {
            return "Insertion Sort";
        }

        public override void Sort(List<ImageModel> images)
        {
            if (images.Count == 0)
                return;

            for(int i = 0; i < images.Count; ++i)
            {
                int j = i;
                while(j > 0 && images[j - 1].Id > images[j].Id)
                {
                    var tmp = images[j - 1];
                    images[j - 1] = images[j];
                    images[j] = tmp;
                    j -= 1;
                }
            }
            return;
        }
    }
}
