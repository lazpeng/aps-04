﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APS_04
{
    class MergeSorter : ISorter
    {
        public override string GetName()
        {
            return "Merge Sort";
        }

        private void Merge(List<ImageModel> target, int offset, int middle, int end)
        {
            int fSize = middle - offset + 1;
            int sSize = end - middle;

            var first = new List<ImageModel>();
            var second = new List<ImageModel>();

            for (int idx = 0; idx < fSize; ++idx)
                first.Add(target[offset + idx]);

            for (int idx = 0; idx < sSize; ++idx)
                second.Add(target[middle + 1 + idx]);

            int i = 0, j = 0;
            int k = offset;

            while(i < fSize && j < sSize)
            {
                if(first[i].Id <= second[j].Id)
                {
                    target[k] = first[i];
                    i += 1;
                } else
                {
                    target[k] = second[j];
                    j += 1;
                }
                k += 1;
            }

            while(i < fSize)
            {
                target[k] = first[i];
                k += 1;
                i += 1;
            }

            while(j < sSize)
            {
                target[k] = second[j];
                k += 1;
                j += 1;
            }
        }

        private void Sort(List<ImageModel> images, int offset, int end)
        {
            if(offset < end)
            {
                int middle = (offset + end) / 2;

                Sort(images, offset, middle);
                Sort(images, middle + 1, end);

                Merge(images, offset, middle, end);
            }
        }

        public override void Sort(List<ImageModel> images)
        {
            Sort(images, 0, images.Count - 1);
            return;
        }
    }
}
