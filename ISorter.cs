﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APS_04
{
    public abstract class ISorter
    {
        public abstract void Sort(List<ImageModel> images);

        public abstract string GetName();
    }
}
