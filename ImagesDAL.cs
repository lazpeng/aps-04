﻿using System;
using System.Collections.Generic;
using System.Text;
using Npgsql;

namespace APS_04
{
    public class ImagesDAL
    {

        private bool initialized = false;
        private string connectionString = "";
        private readonly Random rng = new Random();

        private NpgsqlConnection CreateConnection()
        {
            var conn = new NpgsqlConnection(connectionString);
            conn.Open();
            return conn;
        }

        public void Initialize(string connString)
        {
            connectionString = connString;

            using (var conn = CreateConnection())
            {
                using (var cmd = new NpgsqlCommand("", conn))
                {
                    cmd.CommandText = "CREATE TABLE IF NOT EXISTS IMGCONTENT(ID SERIAL, NAME VARCHAR(250), CONTENT bytea, " +
                        "CONSTRAINT IMGCONTENT_PK PRIMARY KEY (ID), CONSTRAINT IMGCONTENT_UNIQUE UNIQUE (NAME))";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "CREATE TABLE IF NOT EXISTS IMGHEADER(ID BIGINT, CONTENT_ID BIGINT, " +
                        "CONSTRAINT IMGHEADER_PK PRIMARY KEY(ID), CONSTRAINT IMGHEADER_FK FOREIGN KEY (CONTENT_ID) REFERENCES IMGCONTENT(ID))";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "DELETE FROM IMGHEADER";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "DELETE FROM IMGCONTENT";
                    cmd.ExecuteNonQuery();
                }
            }

            initialized = true;
        }

        private bool IsIdAvailable(long id)
        {
            var query = "SELECT COUNT(1) FROM IMGHEADER WHERE ID=:ID";
            using (var conn = CreateConnection())
            {
                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter(":ID", System.Data.DbType.Int64) { Value = id });

                    return Convert.ToInt32(cmd.ExecuteScalar()) == 0;
                }
            }
        }

        private long FindImageByName(string name)
        {
            long result = -1;
            var query = "SELECT ID FROM IMGCONTENT WHERE NAME=:NAME";

            using (var conn = CreateConnection())
            {
                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    cmd.Parameters.Add(new NpgsqlParameter(":NAME", System.Data.DbType.String) { Value = name });

                    var reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        result = reader.GetInt64(0);
                    }
                }
            }

            return result;
        }

        private void SetImageId(ImageModel image)
        {
            long id = FindImageByName(image.Name);

            if (id < 0)
            {
                var query = "INSERT INTO IMGCONTENT(NAME, CONTENT) VALUES(:NAME, :CONTENT)";

                using (var conn = CreateConnection())
                {
                    using (var cmd = new NpgsqlCommand(query, conn))
                    {
                        cmd.Parameters.Add(new NpgsqlParameter(":NAME", System.Data.DbType.String) { Value = image.Name });
                        cmd.Parameters.Add(new NpgsqlParameter(":CONTENT", System.Data.DbType.Object) { Value = image.Content });

                        cmd.ExecuteNonQuery();
                    }
                }

                id = FindImageByName(image.Name);
            }

            image.ContentId = id;
        }

        public void SaveWithRandomId(ImageModel image)
        {
            if (!initialized)
                throw new Exception("DB is not initialized");

            SetImageId(image);

            var query = "INSERT INTO IMGHEADER(ID, CONTENT_ID) VALUES(:ID, :CONTENT_ID)";
            using (var conn = CreateConnection())
            {
                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    long id;

                    do
                    {
                        id = rng.Next(0, 10000);
                    } while (!IsIdAvailable(id));

                    cmd.Parameters.Add(new NpgsqlParameter(":ID", System.Data.DbType.Int64) { Value = id });
                    cmd.Parameters.Add(new NpgsqlParameter(":CONTENT_ID", System.Data.DbType.Int64) { Value = image.ContentId });

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<ImageModel> GetAll()
        {
            if (!initialized)
                throw new Exception("DB is not initialized");

            var images = new List<ImageModel>();
            var query = "SELECT H.ID, NAME FROM IMGHEADER AS H INNER JOIN IMGCONTENT AS C ON H.CONTENT_ID = C.ID";

            using (var conn = CreateConnection())
            {
                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        images.Add(new ImageModel()
                        {
                            Id = reader.GetInt64(0),
                            Name = reader.GetString(1),
                        });
                    }
                }

                return images;
            }
        }
    }
}
