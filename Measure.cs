﻿using System;
using System.Diagnostics;

namespace APS_04
{
    public abstract class Measurement
    {
        /// <summary>
        /// Returns the number of milliseconds <paramref name="action"/> used to execute
        /// </summary>
        /// <param name="action">Function to be measured</param>
        /// <returns>Number of milliseconds</returns>
        public static long Measure(Action action)
        {
            var watch = Stopwatch.StartNew();
            action.Invoke();
            return watch.ElapsedMilliseconds;
        }
    }
}
