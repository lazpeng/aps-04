﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace APS_04
{
    class Program
    {
        private static readonly List<string> extensions = new List<string> { ".PNG", ".JPG", ".JPEG" };

        private static void PrintHeading()
        {
            for (int i = 0; i < 45; ++i)
                Console.Write("=");
            Console.WriteLine();
        }

        private static int CountImageFiles(DirectoryInfo info)
        {
            return new List<FileInfo>(info.GetFiles()).Where((i) => extensions.Contains(i.Extension.ToUpper())).Count();
        }

        static void Main(string[] _)
        {
            string directory = "<não selecionado>";
            int imagesAvailable = 0;
            int targetImageCount = 1000;

            SortProvider.Provider[] providers = new SortProvider.Provider[] 
            {
                SortProvider.Provider.BucketSort, 
                SortProvider.Provider.InsertionSort, 
                SortProvider.Provider.MergeSort 
            };

            while (true)
            {
                PrintHeading();

                Console.WriteLine($"Diretório com imagens: {directory}");
                Console.WriteLine($"Imagens (.png, .jpg/jpeg) disponíveis no diretório: {imagesAvailable}");
                Console.WriteLine($"Quantidade de imagens a serem usadas na medição: {targetImageCount}");

                Console.WriteLine("Digite uma opção:");
                Console.WriteLine("\t1 - Configurar diretório com imagens");
                Console.WriteLine("\t2 - Configurar quantidade de imagens usadas");
                Console.WriteLine("\t3 - Medir execução dos algorítimos de ordenação");
                Console.WriteLine("\t0 - Sair");
                Console.Write(">");

                string answer = Console.ReadLine();

                PrintHeading();

                try
                {
                    switch (answer)
                    {
                        case "1":
                            Console.WriteLine("Digite o caminho a ser usado para carregar as imagens");
                            Console.Write(">");
                            answer = Console.ReadLine();

                            var dir = new DirectoryInfo(answer);
                            if (dir == null || ! dir.Exists)
                            {
                                Console.WriteLine("Diretório especificado é inválido");
                            }
                            else
                            {
                                directory = answer;
                                imagesAvailable = CountImageFiles(dir);
                            }
                            break;
                        case "2":
                            Console.WriteLine("Digite a quantidade de imagens a serem usadas para a medição.");
                            Console.WriteLine("Se o número for maior que o disponível no diretório especificado, as imagens serão repetidas.");
                            Console.WriteLine("Se o número for menor que o disponível, apenas as primeiras n imagens carregadas serão usadas.");
                            Console.Write(">");

                            if(int.TryParse(Console.ReadLine(), out int total))
                            {
                                if (total < 0)
                                {
                                    Console.WriteLine("Digite uma quantidade positiva");
                                }
                                else if (total == 0)
                                {
                                    Console.WriteLine("Digite uma quantidade maior que zero");
                                } else if(total > 10000)
                                {
                                    Console.WriteLine("Quantidade maior que o permitido (10.000)");
                                } else
                                {
                                    targetImageCount = total;
                                }
                            } else
                            {
                                Console.WriteLine("Digite um número válido");
                            }
                            break;
                        case "3":
                            var info = new DirectoryInfo(directory);
                            if (info == null || !info.Exists)
                            {
                                throw new Exception("Diretório especificado é inválido");
                            }

                            imagesAvailable = CountImageFiles(info);
                            var db = new ImagesDAL();
                            db.Initialize(ConfigurationManager.ConnectionStrings["Database"].ConnectionString);

                            int savedImages = 1;

                            var files = new List<FileInfo>(info.GetFiles()).Where((f) => extensions.Contains(f.Extension.ToUpper())).ToList();

                            int fileIndex = 0;
                            for(int i = 0; i < targetImageCount; ++i)
                            {
                                if(fileIndex >= files.Count)
                                {
                                    fileIndex = 0;
                                }

                                Console.CursorLeft = 0;
                                Console.Write($"Salvando imagem {savedImages} no banco...");
                                Console.Out.Flush();
                                savedImages += 1;

                                var file = files[fileIndex];
                                fileIndex += 1;

                                var image = new ImageModel()
                                {
                                    Name = file.Name,
                                    Content = new byte[file.Length]
                                };

                                file.OpenRead().Read(image.Content, 0, image.Content.Count());
                                db.SaveWithRandomId(image);
                            }

                            Console.WriteLine();
                            PrintHeading();

                            var measurements = new List<KeyValuePair<string, long>>();

                            foreach (var kind in providers)
                            {
                                var sorter = SortProvider.GetSorter(kind);
                                var sorterName = sorter.GetName();

                                Console.Write($"Medindo {sorterName}...");
                                Console.Out.Flush();
                                var time = Measurement.Measure(() => sorter.Sort(db.GetAll()));
                                Console.CursorLeft = 0;
                                Console.WriteLine($"{sorterName} levou {time} milisegundos.");

                                measurements.Add(new KeyValuePair<string, long>(sorterName, time));
                            }

                            measurements.Sort((l, r) => l.Value.CompareTo(r.Value));

                            Console.WriteLine($"Foram processadas {imagesAvailable} imagens por todos os algorítimos");
                            Console.WriteLine("Resultados (melhor ao pior)");
                            for(int i = 0; i < measurements.Count; ++i)
                            {
                                Console.WriteLine($"\t{i + 1}º - {measurements[i].Key}, {measurements[i].Value} milisegundos");
                            }
                            break;
                        case "0":
                            return;
                        default:
                            Console.WriteLine($"Opção {answer} não compreendida");
                            break;
                    }
                } catch (Exception e)
                {
                    Console.WriteLine($"Ocorreu um erro durante a execução: {e.Message}\n{e.StackTrace}");
                }
            }
        }
    }
}
