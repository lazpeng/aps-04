﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APS_04
{
    public class ImageModel
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public byte[] Content { get; set; }
        public long ContentId { get; set; }
    }
}
