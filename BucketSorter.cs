﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APS_04
{
    class BucketSorter : ISorter
    {
        public override string GetName()
        {
            return "Bucket Sort";
        }

        public override void Sort(List<ImageModel> images)
        {
            var buckets = new List<List<ImageModel>>();
            int size = images.Count;
            long maxVal = 0;
            
            foreach(var i in images)
            {
                if (i.Id > maxVal)
                    maxVal = i.Id;
            }

            for(int i = 0; i < maxVal; ++i)
            {
                buckets.Add(new List<ImageModel>());
            }

            for(int i = 0; i < size; ++i)
            {
                int index = (int)Math.Floor((double)(size * images[i].Id / maxVal));
                buckets[index].Add(images[i]);
            }

            images.Clear();

            for(int i = 0; i < buckets.Count; ++i)
            {
                new InsertionSorter().Sort(buckets[i]);

                for(int j = 0; j < buckets[i].Count; ++j)
                {
                    images.Add(buckets[i][j]);
                }
            }

            return;
        }
    }
}
